﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    /// <summary>
    /// The <see cref="BaseForm"/> provides a simple way for inheriting forms
    /// to handle disposing arbitrary <see cref="IDisposable"/> objects.
    /// </summary>
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory("Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public class BaseForm : Form
    {
        protected virtual void HandleDispose(bool disposing) { }

        protected override void Dispose(bool disposing)
        {
            this.HandleDispose(disposing);
            base.Dispose(disposing);
        }
    }
}
