﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    /// <summary>
    /// Helper class related to <see cref="Control"/> rendering, similar to the windows forms <see cref="ControlPaint"/> helper class.
    /// </summary>
    public static class PaintHelper
    {
        /// <summary>
        /// Returns the color used for disabled text based on the <paramref name="backColor"/> they will be drawn over.
        /// </summary>
        /// <param name="backColor">The color behind the text.</param>
        public static Color DisabledTextColor(Color backColor) => (!PaintHelper.IsDarker(backColor, SystemColors.Control) ? SystemColors.ControlDark : ControlPaint.Dark(backColor));

        /// <summary>
        /// Returns whether the <paramref name="left"/> color is darker than the <paramref name="right"/> color.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public static bool IsDarker(Color left, Color right) => (new HLSColor(left).Luminosity < new HLSColor(right).Luminosity);
    }

    internal struct HLSColor
    {
        private const int _shadowAdj = -333;
        private const int _hilightAdj = 500;
        private const int _watermarkAdj = -50;

        private const int _range = 240;
        private const int _hLSMax = _range;
        private const int _rGBMax = 255;
        private const int _undefined = _hLSMax * 2 / 3;
        private readonly bool _isSystemColors_Control;

        public HLSColor(Color color)
        {
            this._isSystemColors_Control = (color.ToKnownColor() == SystemColors.Control.ToKnownColor());
            int r = color.R;
            int g = color.G;
            int b = color.B;
            int max, min;        /* max and min RGB values */
            int sum, dif;
            int Rdelta, Gdelta, Bdelta;  /* intermediate value: % of spread from max */

            /* calculate lightness */
            max = Math.Max(Math.Max(r, g), b);
            min = Math.Min(Math.Min(r, g), b);
            sum = max + min;

            this.Luminosity = (((sum * _hLSMax) + _rGBMax) / (2 * _rGBMax));

            dif = max - min;
            if (dif == 0)
            {       /* r=g=b --> achromatic case */
                this.Saturation = 0;                         /* saturation */
                this.Hue = _undefined;                 /* hue */
            }
            else
            {                           /* chromatic case */
                /* saturation */
                if (this.Luminosity <= (_hLSMax / 2))
                {
                    this.Saturation = (int) (((dif * (int) _hLSMax) + (sum / 2)) / sum);
                }
                else
                {
                    this.Saturation = (int) ((int) ((dif * (int) _hLSMax) + (int) ((2 * _rGBMax - sum) / 2))
                                        / (2 * _rGBMax - sum));
                }
                /* hue */
                Rdelta = (int) ((((max - r) * (int) (_hLSMax / 6)) + (dif / 2)) / dif);
                Gdelta = (int) ((((max - g) * (int) (_hLSMax / 6)) + (dif / 2)) / dif);
                Bdelta = (int) ((((max - b) * (int) (_hLSMax / 6)) + (dif / 2)) / dif);

                if ((int) r == max)
                {
                    this.Hue = Bdelta - Gdelta;
                }
                else if ((int) g == max)
                {
                    this.Hue = (_hLSMax / 3) + Rdelta - Bdelta;
                }
                else /* B == cMax */
                {
                    this.Hue = ((2 * _hLSMax) / 3) + Gdelta - Rdelta;
                }

                if (this.Hue < 0)
                {
                    this.Hue += _hLSMax;
                }

                if (this.Hue > _hLSMax)
                {
                    this.Hue -= _hLSMax;
                }
            }
        }

        public int Hue { get; }

        public int Saturation { get; }

        public int Luminosity { get; }

        public Color Darker(float percDarker)
        {
            if (this._isSystemColors_Control)
            {
                // With the usual color scheme, ControlDark/DarkDark is not exactly
                // what we would otherwise calculate
                if (percDarker == 0.0f)
                {
                    return SystemColors.ControlDark;
                }
                else if (percDarker == 1.0f)
                {
                    return SystemColors.ControlDarkDark;
                }
                else
                {
                    Color dark = SystemColors.ControlDark;
                    Color darkDark = SystemColors.ControlDarkDark;

                    int dr = dark.R - darkDark.R;
                    int dg = dark.G - darkDark.G;
                    int db = dark.B - darkDark.B;

                    return Color.FromArgb((byte) (dark.R - (byte) (dr * percDarker)),
                                          (byte) (dark.G - (byte) (dg * percDarker)),
                                          (byte) (dark.B - (byte) (db * percDarker)));
                }
            }
            else
            {
                int oneLum = 0;
                int zeroLum = this.NewLuma(_shadowAdj, true);

                /*                                        
                if (luminosity < 40) {
                    zeroLum = NewLuma(120, ShadowAdj, true);
                }
                else {
                    zeroLum = NewLuma(ShadowAdj, true);
                }
                */

                return this.ColorFromHLS(this.Hue, zeroLum - (int) ((zeroLum - oneLum) * percDarker), this.Saturation);
            }
        }

        public override bool Equals(object o)
        {
            if (!(o is HLSColor))
            {
                return false;
            }

            HLSColor c = (HLSColor) o;
            return this.Hue == c.Hue &&
                   this.Saturation == c.Saturation &&
                   this.Luminosity == c.Luminosity &&
                   this._isSystemColors_Control == c._isSystemColors_Control;
        }

        public static bool operator ==(HLSColor a, HLSColor b) => a.Equals(b);

        public static bool operator !=(HLSColor a, HLSColor b) => !a.Equals(b);

        public override int GetHashCode() => this.Hue << 6 | this.Saturation << 2 | this.Luminosity;

        public Color Lighter(float percLighter)
        {
            if (this._isSystemColors_Control)
            {
                // With the usual color scheme, ControlLight/LightLight is not exactly
                // what we would otherwise calculate
                if (percLighter == 0.0f)
                {
                    return SystemColors.ControlLight;
                }
                else if (percLighter == 1.0f)
                {
                    return SystemColors.ControlLightLight;
                }
                else
                {
                    Color light = SystemColors.ControlLight;
                    Color lightLight = SystemColors.ControlLightLight;

                    int dr = light.R - lightLight.R;
                    int dg = light.G - lightLight.G;
                    int db = light.B - lightLight.B;

                    return Color.FromArgb((byte) (light.R - (byte) (dr * percLighter)),
                                          (byte) (light.G - (byte) (dg * percLighter)),
                                          (byte) (light.B - (byte) (db * percLighter)));
                }
            }
            else
            {
                int zeroLum = this.Luminosity;
                int oneLum = this.NewLuma(_hilightAdj, true);

                /*
                if (luminosity < 40) {
                    zeroLum = 120;
                    oneLum = NewLuma(120, HilightAdj, true);
                }
                else {
                    zeroLum = luminosity;
                    oneLum = NewLuma(HilightAdj, true);
                }
                */

                return this.ColorFromHLS(this.Hue, zeroLum + (int) ((oneLum - zeroLum) * percLighter), this.Saturation);
            }
        }

        private int NewLuma(int n, bool scale) => this.NewLuma(this.Luminosity, n, scale);

        private int NewLuma(int luminosity, int n, bool scale)
        {
            if (n == 0)
            {
                return luminosity;
            }

            if (scale)
            {
                if (n > 0)
                {
                    return (int) (((int) luminosity * (1000 - n) + (_range + 1L) * n) / 1000);
                }
                else
                {
                    return (int) (((int) luminosity * (n + 1000)) / 1000);
                }
            }

            int newLum = luminosity;
            newLum += (int) ((long) n * _range / 1000);

            if (newLum < 0)
            {
                newLum = 0;
            }

            if (newLum > _hLSMax)
            {
                newLum = _hLSMax;
            }

            return newLum;
        }

        private Color ColorFromHLS(int hue, int luminosity, int saturation)
        {
            byte r, g, b;                      /* RGB component values */
            int magic1, magic2;       /* calculated magic numbers (really!) */

            if (saturation == 0)
            {                /* achromatic case */
                r = g = b = (byte) ((luminosity * _rGBMax) / _hLSMax);
                if (hue != _undefined)
                {
                    /* ERROR */
                }
            }
            else
            {                         /* chromatic case */
                /* set up magic numbers */
                if (luminosity <= (_hLSMax / 2))
                {
                    magic2 = (int) ((luminosity * ((int) _hLSMax + saturation) + (_hLSMax / 2)) / _hLSMax);
                }
                else
                {
                    magic2 = luminosity + saturation - (int) (((luminosity * saturation) + (int) (_hLSMax / 2)) / _hLSMax);
                }

                magic1 = 2 * luminosity - magic2;

                /* get RGB, change units from HLSMax to RGBMax */
                r = (byte) (((this.HueToRGB(magic1, magic2, (int) (hue + (int) (_hLSMax / 3))) * (int) _rGBMax + (_hLSMax / 2))) / (int) _hLSMax);
                g = (byte) (((this.HueToRGB(magic1, magic2, hue) * (int) _rGBMax + (_hLSMax / 2))) / _hLSMax);
                b = (byte) (((this.HueToRGB(magic1, magic2, (int) (hue - (int) (_hLSMax / 3))) * (int) _rGBMax + (_hLSMax / 2))) / (int) _hLSMax);
            }
            return Color.FromArgb(r, g, b);
        }

        private int HueToRGB(int n1, int n2, int hue)
        {
            /* range check: note values passed add/subtract thirds of range */

            /* The following is redundant for WORD (unsigned int) */
            if (hue < 0)
            {
                hue += _hLSMax;
            }

            if (hue > _hLSMax)
            {
                hue -= _hLSMax;
            }

            /* return r,g, or b value from this tridrant */
            if (hue < (_hLSMax / 6))
            {
                return (n1 + (((n2 - n1) * hue + (_hLSMax / 12)) / (_hLSMax / 6)));
            }

            if (hue < (_hLSMax / 2))
            {
                return (n2);
            }

            if (hue < ((_hLSMax * 2) / 3))
            {
                return (n1 + (((n2 - n1) * (((_hLSMax * 2) / 3) - hue) + (_hLSMax / 12)) / (_hLSMax / 6)));
            }
            else
            {
                return (n1);
            }
        }

    }
}
