﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;

namespace CommonCore.Forms
{
    public static class SystemIconEnumExtensions
    {
        public static Icon ToIcon(this SystemIconEnum self)
            => self switch
            {
                SystemIconEnum.Application => SystemIcons.Application,
                SystemIconEnum.Asterisk => SystemIcons.Asterisk,
                SystemIconEnum.Error => SystemIcons.Error,
                SystemIconEnum.Exclamation => SystemIcons.Exclamation,
                SystemIconEnum.Hand => SystemIcons.Hand,
                SystemIconEnum.Information => SystemIcons.Information,
                SystemIconEnum.Question => SystemIcons.Question,
                SystemIconEnum.Shield => SystemIcons.Shield,
                SystemIconEnum.Warning => SystemIcons.Warning,
                SystemIconEnum.WinLogo => SystemIcons.WinLogo,
                _ => null,
            };
    }
}
