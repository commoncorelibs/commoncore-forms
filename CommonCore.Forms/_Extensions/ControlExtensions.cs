﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    public static class ControlExtensions
    {
        public static void Invoke(this Control self, Action action)
        {
            ThrowHelper.IfSelfNull(self);
            self.Invoke(action);
        }

        public static T InvokeIfRequired<T>(this T source, Action<T> action)
            where T : Control
        {
            try
            {
                if (!source.InvokeRequired) { action(source); }
                else { source.Invoke(new Action(() => action(source))); }
            }
            catch (Exception ex)
            { Debug.Write("Error on 'InvokeIfRequired': {0}", ex.Message); }
            return source;
        }

    }
}
