﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommonCore.Forms.Dialogs
{
    public partial class PropertyGridDialog : DialogForm
    {
        /// <summary>
        /// Displays the <see cref="PropertyGridDialog"/> with the specified <paramref name="content"/> text.
        /// </summary>
        /// <param name="handle">Window handle to attach the dialog to or <c>null</c>.</param>
        /// <param name="content">Object content to display by the dialog.</param>
        public static void DisplayDialog(IWin32Window handle, object content, bool @readonly = false)
        {
            using var dialog = new PropertyGridDialog()
            { SelectedObject = content, ReadOnly = @readonly };
            dialog.ShowDialog(handle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyGridDialog"/> class.
        /// </summary>
        public PropertyGridDialog()
        {
            this.InitializeComponent();
            //this._grid.HelpVisible = false;
            //this._grid.ToolbarVisible = false;
            //this._grid.CommandsVisibleIfAvailable = false;
            //this._grid.PropertySort = PropertySort.NoSort;
        }

        /// <summary>
        /// Gets the <see cref="PropertyGrid"/> used to display the content of the dialog.
        /// </summary>
        public PropertyGrid PropertyGrid => this._grid;

        public bool ReadOnly
        {
            get => this._readOnly;
            set
            {
                if (this._readOnly != value)
                {
                    this._readOnly = value;
                    this._grid.DisabledItemForeColor = Color.FromKnownColor(value ? KnownColor.ControlText : KnownColor.GrayText);
                }
            }
        }
        private bool _readOnly;

        [DefaultValue(true)]
        public bool ContextMenuEnabled { get; set; } = true;

        /// <summary>
        /// Gets or sets the property grid content of the dialog.
        /// </summary>
        public object SelectedObject { get => this._grid.SelectedObject; set => this._grid.SelectedObject = value; }

        private void _context_Opening(object sender, CancelEventArgs e)
        {
            if (this.ReadOnly || !this.ContextMenuEnabled) { e.Cancel = true; return; }
            var prop = this._grid.SelectedGridItem.PropertyDescriptor;
            //if (prop == null) { e.Cancel = true; return; }
            this._contextReset.Enabled = prop is not null && prop.CanResetValue(this._grid.SelectedObject);
        }

        private void _contextReset_Click(object sender, EventArgs e)
            => this._grid.ResetSelectedProperty();
    }
}
