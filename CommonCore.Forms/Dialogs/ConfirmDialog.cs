﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Windows.Forms;

namespace CommonCore.Forms.Dialogs
{
    /// <summary>
    /// The <see cref="ConfirmDialog"/> provides a quick mechanism to request
    /// confirmation from the user.
    /// </summary>
    public partial class ConfirmDialog : DialogForm
    {
        /// <summary>
        /// Displays the <see cref="ConfirmDialog"/> and returns <c>true</c> if they accepted or <c>false</c> if they refused.
        /// </summary>
        /// <param name="handle">Window handle to attach the dialog to or <c>null</c>.</param>
        /// <param name="message">Message to display on the dialog or <c>null</c> for the default message.</param>
        public static bool DisplayDialog(IWin32Window handle, string message = null)
        {
            using var dialog = new ConfirmDialog();
            if (message != null) { dialog.Message = message; }
            return DialogResult.OK == dialog.ShowDialog(handle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfirmDialog"/> class.
        /// </summary>
        public ConfirmDialog() => this.InitializeComponent();

        /// <summary>
        /// Gets or sets the message that is displayed on the dialog.
        /// </summary>
        public string Message { get => this._label.Text; set => this._label.Text = value; }
    }
}
