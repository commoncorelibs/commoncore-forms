﻿namespace CommonCore.Forms.Dialogs
{
    partial class ConfirmDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._buttonAccept = new System.Windows.Forms.Button();
            this._buttonCancel = new System.Windows.Forms.Button();
            this._label = new System.Windows.Forms.Label();
            this._table.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._table.ColumnCount = 2;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._table.Controls.Add(this._buttonAccept, 0, 1);
            this._table.Controls.Add(this._buttonCancel, 1, 1);
            this._table.Controls.Add(this._label, 0, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Margin = new System.Windows.Forms.Padding(0);
            this._table.Name = "_table";
            this._table.RowCount = 2;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._table.Size = new System.Drawing.Size(284, 56);
            this._table.TabIndex = 0;
            // 
            // _buttonAccept
            // 
            this._buttonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._buttonAccept.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonAccept.Location = new System.Drawing.Point(0, 33);
            this._buttonAccept.Margin = new System.Windows.Forms.Padding(0);
            this._buttonAccept.Name = "_buttonAccept";
            this._buttonAccept.Size = new System.Drawing.Size(142, 23);
            this._buttonAccept.TabIndex = 0;
            this._buttonAccept.Text = "&Accept";
            this._buttonAccept.UseVisualStyleBackColor = true;
            // 
            // _buttonCancel
            // 
            this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._buttonCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonCancel.Location = new System.Drawing.Point(142, 33);
            this._buttonCancel.Margin = new System.Windows.Forms.Padding(0);
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.Size = new System.Drawing.Size(142, 23);
            this._buttonCancel.TabIndex = 0;
            this._buttonCancel.Text = "&Cancel";
            this._buttonCancel.UseVisualStyleBackColor = true;
            // 
            // _label
            // 
            this._label.AutoSize = true;
            this._table.SetColumnSpan(this._label, 2);
            this._label.Location = new System.Drawing.Point(5, 10);
            this._label.Margin = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(220, 13);
            this._label.TabIndex = 1;
            this._label.Text = "This action cannot be undone. Are you sure?";
            // 
            // ConfirmDialog
            // 
            this.AcceptButton = this._buttonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this._buttonCancel;
            this.ClientSize = new System.Drawing.Size(284, 56);
            this.Controls.Add(this._table);
            this.MinimumSize = new System.Drawing.Size(300, 95);
            this.Name = "ConfirmDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Confirm Action";
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _table;
        private System.Windows.Forms.Button _buttonAccept;
        private System.Windows.Forms.Button _buttonCancel;
        private System.Windows.Forms.Label _label;
    }
}