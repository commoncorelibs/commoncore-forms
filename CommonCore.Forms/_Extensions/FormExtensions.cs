﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    public static class FormExtensions
    {
        /// <summary>
        /// Set the Dialog Form's title bar icon.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="icon">The icon to use.</param>
        public static void SetIcon(this Form self, Icon icon) => self.Icon = icon;

        /// <summary>
        /// Set the Dialog Form's title bar icon to the icon of another form.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="other">The form to get the icon from.</param>
        public static void SetIcon(this Form self, Form other) => self.Icon = other.Icon;

        /// <summary>
        /// Set the Dialog Form's title bar icon from an image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="image">The image to use.</param>
        // TODO: Test this, looks like it disposes the icon after setting it.
        public static void SetIcon(this Form self, Bitmap image)
        {
            using var icon = Icon.FromHandle(image.GetHicon());
            self.Icon = icon;
        }

        /// <summary>
        /// Sets the dialog icon to one of the <see cref="SystemIconEnum"/> values.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value"><see cref="SystemIconEnum"/> value representing the system icon.</param>
        public static void SetIcon(this Form self, SystemIconEnum value) => self.Icon = value.ToIcon();
    }
}
