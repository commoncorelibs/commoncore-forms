# Vinland Solutions CommonCore.Forms Library

[![License](https://commoncorelibs.gitlab.io/commoncore-forms/badge/license.svg)](https://commoncorelibs.gitlab.io/commoncore-forms/license.html)
[![Platform](https://commoncorelibs.gitlab.io/commoncore-forms/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://commoncorelibs.gitlab.io/commoncore-forms/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-forms)
[![Releases](https://commoncorelibs.gitlab.io/commoncore-forms/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-forms/-/releases)
[![Documentation](https://commoncorelibs.gitlab.io/commoncore-forms/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-forms/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.forms/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Forms/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-forms/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-forms/pipelines)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-forms/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-forms/reports/index.html)
[![Tests](https://commoncorelibs.gitlab.io/commoncore-forms/badge/tests.svg)](https://commoncorelibs.gitlab.io/commoncore-forms/reports/index.html)

**CommonCore.Forms** is a .Net 5 Windows Forms library ?.

## Installation

The official release versions of the **CommonCore.Forms** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Forms/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

## Usage

TODO: Text here.

## Projects

The **CommonCore.Forms** repository is composed of three projects with the listed dependencies:

* **CommonCore.Forms**: The dotnet 5 windows forms library project.
  * [CommonCore](https://gitlab.com/commoncorelibs/commoncore)
  * [CommonCore.Drawing](https://gitlab.com/commoncorelibs/commoncore-drawing)
  * [CommonCore.Exceptions](https://gitlab.com/commoncorelibs/commoncore-exceptions)
* **CommonCore.Forms.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.Forms.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-forms
- Issues: https://gitlab.com/commoncorelibs/commoncore-forms/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-forms/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Forms/

## Credits

These two libraries are used by the repo's CICD pipeline and are not a part of the library itself.

* [AnyBadge](https://github.com/jongracecox/anybadge)
* [semver](https://github.com/k-bx/python-semver)
