﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;
using System.Windows.Forms;

namespace CommonCore.Forms.Dialogs
{
    /// <summary>
    /// The <see cref="DisplayImageDialog"/> provides a quick way to display an <see cref="Image"/> to the user.
    /// </summary>
    public partial class DisplayImageDialog : DialogForm
    {
        /// <summary>
        /// Displays the <see cref="DisplayImageDialog"/> with the specified <paramref name="content"/> text.
        /// </summary>
        /// <param name="handle">Window handle to attach the dialog to or <c>null</c>.</param>
        /// <param name="content">Text content to display by the dialog.</param>
        public static void DisplayDialog(IWin32Window handle, Image content)
        {
            using var dialog = new DisplayImageDialog()
            { Image = content };
            dialog.ShowDialog(handle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayImageDialog"/> class.
        /// </summary>
        public DisplayImageDialog() => this.InitializeComponent();

        /// <summary>
        /// Gets the <see cref="PictureBox"/> used to display an <see cref="Image"/> on the <see cref="DisplayImageDialog"/>.
        /// </summary>
        public PictureBox PictureBox => this._image;

        /// <summary>
        /// Gets or sets the image content of the dialog.
        /// </summary>
        public Image Image { get => this._image.Image; set => this._image.Image = value; }

        /// <summary>
        /// Gets or sets the initial image for the content of the dialog.
        /// </summary>
        public Image InitialImage { get => this._image.InitialImage; set => this._image.InitialImage = value; }

        /// <summary>
        /// Gets or sets the error image for the content of the dialog.
        /// </summary>
        public Image ErrorImage { get => this._image.ErrorImage; set => this._image.ErrorImage = value; }

        /// <summary>
        /// Gets or sets the location of the image content of the dialog.
        /// </summary>
        public string ImageLocation { get => this._image.ImageLocation; set => this._image.ImageLocation = value; }

        /// <summary>
        /// Gets or sets the size mode of the image content of the dialog.
        /// </summary>
        public PictureBoxSizeMode ImageSizeMode { get => this._image.SizeMode; set => this._image.SizeMode = value; }
    }
}
