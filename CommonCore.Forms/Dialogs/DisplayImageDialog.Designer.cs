﻿namespace CommonCore.Forms.Dialogs
{
    partial class DisplayImageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._buttonClose = new System.Windows.Forms.Button();
            this._image = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._image)).BeginInit();
            this.SuspendLayout();
            // 
            // _buttonClose
            // 
            this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._buttonClose.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonClose.Location = new System.Drawing.Point(0, 234);
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.Size = new System.Drawing.Size(284, 27);
            this._buttonClose.TabIndex = 2;
            this._buttonClose.Text = "&Close";
            this._buttonClose.UseVisualStyleBackColor = true;
            // 
            // _image
            // 
            this._image.Dock = System.Windows.Forms.DockStyle.Fill;
            this._image.Location = new System.Drawing.Point(0, 0);
            this._image.Name = "_image";
            this._image.Size = new System.Drawing.Size(284, 234);
            this._image.TabIndex = 3;
            this._image.TabStop = false;
            // 
            // DisplayImageDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._buttonClose;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this._image);
            this.Controls.Add(this._buttonClose);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "DisplayImageDialog";
            this.Text = "ImageDialog";
            ((System.ComponentModel.ISupportInitialize)(this._image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _buttonClose;
        private System.Windows.Forms.PictureBox _image;
    }
}