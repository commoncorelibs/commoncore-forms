﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using CommonCore.Drawing;

namespace CommonCore.Forms
{
    /// <summary>
    /// A <see cref="Panel"/> class intended to serve as a drawing surface, with optimal
    /// <see cref="ControlStyles"/> set in the constructor. Specifically:
    /// <see cref="ControlStyles.UserPaint" />,
    /// <see cref="ControlStyles.AllPaintingInWmPaint" />,
    /// <see cref="ControlStyles.OptimizedDoubleBuffer" />,
    /// <see cref="ControlStyles.ResizeRedraw" />,
    /// and <see cref="ControlStyles.SupportsTransparentBackColor" />.
    /// </summary>
    [DefaultEvent("Paint")]
    [DefaultProperty("BorderStyle")]
    [Designer("System.Windows.Forms.Design.PanelDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [Docking(DockingBehavior.Ask)]
    [Description("A Panel class intended to serve as a drawing surface, with optimal ControlStyles set in the constructor.")]
    public class CanvasPanel : Panel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CanvasPanel"/> class and updates
        /// the control with the optimal <see cref="ControlStyles"/> enabled.
        /// Specifically:
        /// <see cref="ControlStyles.UserPaint" />,
        /// <see cref="ControlStyles.AllPaintingInWmPaint" />,
        /// <see cref="ControlStyles.OptimizedDoubleBuffer" />,
        /// <see cref="ControlStyles.ResizeRedraw" />,
        /// and <see cref="ControlStyles.SupportsTransparentBackColor" />.
        /// </summary>
        public CanvasPanel() : base()
        {
            base.SetStyle(ControlStyles.UserPaint, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);
            base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            base.UpdateStyles();
        }

        [Browsable(false)]
        public Point PrevMouseLocation { get; protected set; }

        [Browsable(false)]
        public bool MouseMoved { get; protected set; }

        public ToolTip? ToolTip { get; set; }

        /// <summary>
        /// Gets the <see cref="Control.ClientRectangle"/> deflated by the <see cref="Control.Padding"/>.
        /// Unlike <see cref="Control.DisplayRectangle"/>, the result of this property
        /// does not consider any scrolling of the panel.
        /// </summary>
        public Rectangle PaddedRectangle => this.ClientRectangle.ToDeflated(this.Padding);

        /// <summary>
        /// Gets the <see cref="Control.ClientRectangle"/> with the point offset by
        /// <see cref="ScrollableControl.AutoScrollPosition"/> and with the width and
        /// height as the max between <see cref="Control.ClientSize"/> and
        /// <see cref="ScrollableControl.AutoScrollMinSize"/>.
        /// Unlike <see cref="Control.DisplayRectangle"/>, the result of this property
        /// does not consider any padding of the panel.
        /// </summary>
        public Rectangle ScrolledRectangle
            => this.ClientRectangle
                .WithDeltaPoint(this.AutoScrollPosition)
                .WithSize(
                    this.AutoScrollMinSize.Width.Max(this.ClientSize.Width),
                    this.AutoScrollMinSize.Height.Max(this.ClientSize.Height))
                ;

        protected virtual void HandleToolTip(MouseEventArgs e) { }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            this.MouseMoved = e.Location != this.PrevMouseLocation;
            if (this.MouseMoved) { this.PrevMouseLocation = e.Location; }
            else { return; }

            if (this.ToolTip is not null) { this.HandleToolTip(e); }
        }
    }
}
