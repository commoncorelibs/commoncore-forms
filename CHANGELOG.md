## 1.0.0-pre.2

* Build: Upgrade CommonCore.Drawing 1.0.0-pre.2 to 1.0.0-pre.3.
* Build: Upgrade CommonCore.Exceptions 1.0.0-pre.1 to 1.0.0-pre.3.

## 1.0.0-pre.1

* Fix: `DialogForm` classes not centering on parent form. #4
* Refactor: Port `DialogForm.SetIcon()` methods to `Form` extension methods. #3
  * Ported `SystemIconEnum` from `CommonCore.Forms.Dialogs` to `CommonCore.Forms`.
  * Implemented `SystemIconEnumExtensions.ToIcon()` extension method to map enum value to `Icon`.
* Add: Implement `TextBoxExtensions.AppendLine()` methods. #5
* Add: Implement `ControlExtensions` invoke method to coerce lambdas. #2
* Style: Organized various files into appropriate folders.

## 1.0.0

* Initial project creation.
