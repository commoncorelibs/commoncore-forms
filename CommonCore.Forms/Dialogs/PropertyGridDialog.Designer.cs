﻿
namespace CommonCore.Forms.Dialogs
{
    partial class PropertyGridDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._buttonClose = new System.Windows.Forms.Button();
            this._grid = new System.Windows.Forms.PropertyGrid();
            this._context = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._contextReset = new System.Windows.Forms.ToolStripMenuItem();
            this._context.SuspendLayout();
            this.SuspendLayout();
            // 
            // _buttonClose
            // 
            this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._buttonClose.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonClose.Location = new System.Drawing.Point(0, 234);
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.Size = new System.Drawing.Size(284, 27);
            this._buttonClose.TabIndex = 2;
            this._buttonClose.Text = "&Close";
            this._buttonClose.UseVisualStyleBackColor = true;
            // 
            // _grid
            // 
            this._grid.CommandsVisibleIfAvailable = false;
            this._grid.ContextMenuStrip = this._context;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.HelpVisible = false;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this._grid.Size = new System.Drawing.Size(284, 234);
            this._grid.TabIndex = 3;
            this._grid.ToolbarVisible = false;
            // 
            // _context
            // 
            this._context.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._contextReset});
            this._context.Name = "_context";
            this._context.Size = new System.Drawing.Size(103, 26);
            this._context.Opening += new System.ComponentModel.CancelEventHandler(this._context_Opening);
            // 
            // _contextReset
            // 
            this._contextReset.Name = "_contextReset";
            this._contextReset.Size = new System.Drawing.Size(102, 22);
            this._contextReset.Text = "&Reset";
            this._contextReset.Click += new System.EventHandler(this._contextReset_Click);
            // 
            // PropertyGridDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this._grid);
            this.Controls.Add(this._buttonClose);
            this.Name = "PropertyGridDialog";
            this.Text = "PropertyGridDialog";
            this._context.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _buttonClose;
        private System.Windows.Forms.PropertyGrid _grid;
        private System.Windows.Forms.ContextMenuStrip _context;
        private System.Windows.Forms.ToolStripMenuItem _contextReset;
    }
}