﻿namespace CommonCore.Forms.Dialogs
{
    partial class DisplayTextDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._text = new System.Windows.Forms.TextBox();
            this._buttonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _text
            // 
            this._text.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._text.Dock = System.Windows.Forms.DockStyle.Fill;
            this._text.Location = new System.Drawing.Point(0, 0);
            this._text.Multiline = true;
            this._text.Name = "_text";
            this._text.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._text.Size = new System.Drawing.Size(284, 234);
            this._text.TabIndex = 0;
            // 
            // _buttonClose
            // 
            this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._buttonClose.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonClose.Location = new System.Drawing.Point(0, 234);
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.Size = new System.Drawing.Size(284, 27);
            this._buttonClose.TabIndex = 1;
            this._buttonClose.Text = "&Close";
            this._buttonClose.UseVisualStyleBackColor = true;
            // 
            // DisplayTextDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._buttonClose;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this._text);
            this.Controls.Add(this._buttonClose);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "DisplayTextDialog";
            this.Text = "TextDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _text;
        private System.Windows.Forms.Button _buttonClose;
    }
}