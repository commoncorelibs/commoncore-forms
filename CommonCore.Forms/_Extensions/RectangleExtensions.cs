﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.Rectangle" /> and <see cref="System.Drawing.RectangleF" /> extension methods related to <see cref="System.Windows.Forms"/> types.
    /// </summary>
    public static class RectangleExtensions
    {
        public static Rectangle ToInflated(this Rectangle self, Padding padding)
            => new(self.X - padding.Left, self.Y - padding.Top, self.Width + padding.Horizontal, self.Height + padding.Vertical);

        public static Rectangle ToDeflated(this Rectangle self, Padding padding)
            => new(self.X + padding.Left, self.Y + padding.Top, self.Width - padding.Horizontal, self.Height - padding.Vertical);
    }
}
