﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;

namespace CommonCore.Forms.Dialogs
{
    /// <summary>
    /// Represents a dialog box that is launched by an application.
    /// </summary>
    //[ClassInterface(ClassInterfaceType.AutoDispatch)]
    //[ComVisible(true)]
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory("Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public class DialogForm : BaseForm
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public DialogForm()
        {
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterParent;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the form is displayed in the Windows taskbar.
        /// </summary>
        [DefaultValue(false)]
        [Category("Window Style")]
        [Description("Determines whether the form is displayed in the Windows taskbar.")]
        public new bool ShowInTaskbar { get => base.ShowInTaskbar; set => base.ShowInTaskbar = value; }

        /// <summary>
        /// Gets or sets the starting position of the form at run time.
        /// </summary>
        /// <exception cref="System.ComponentModel.InvalidEnumArgumentException">The value specified is outside the range of valid values.</exception>
        [DefaultValue(FormStartPosition.CenterParent)]
        [Localizable(true)]
        [Category("Layout")]
        [Description("The starting position of the form at run time.")]
        public new FormStartPosition StartPosition { get => base.StartPosition; set => base.StartPosition = value; }
    }
}
