﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Forms
{
    public enum SystemIconEnum : int
    {
        Application,
        Asterisk,
        Error,
        Exclamation,
        Hand,
        Information,
        Question,
        Shield,
        Warning,
        WinLogo
    }
}
