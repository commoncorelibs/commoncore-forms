﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Windows.Forms;

namespace CommonCore.Forms.Dialogs
{
    /// <summary>
    /// The <see cref="DisplayTextDialog"/> provides a simple way to request text from or display text to the user.
    /// </summary>
    public partial class DisplayTextDialog : DialogForm
    {
        /// <summary>
        /// Displays the <see cref="DisplayTextDialog"/> with the specified <paramref name="content"/> text.
        /// </summary>
        /// <param name="handle">Window handle to attach the dialog to or <c>null</c>.</param>
        /// <param name="content">Text content to display by the dialog.</param>
        public static void DisplayDialog(IWin32Window handle, string content)
        {
            using var dialog = new DisplayTextDialog()
            { Content = content ?? string.Empty };
            dialog.ShowDialog(handle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayTextDialog"/> class.
        /// </summary>
        public DisplayTextDialog() => this.InitializeComponent();

        /// <summary>
        /// Gets the <see cref="TextBox"/> used to display the text content of the dialog.
        /// </summary>
        public TextBox TextBox => this._text;

        /// <summary>
        /// Gets or sets the text content of the dialog.
        /// </summary>
        public string Content { get => this._text.Text; set => this._text.Text = value; }

        /// <summary>
        /// Gets or sets whether the text content is readonly.
        /// </summary>
        public bool ReadOnly { get => this._text.ReadOnly; set => this._text.ReadOnly = value; }

        public void SelectNone() => this.TextBox.Select(this.TextBox.SelectionStart, 0);

        public void SelectAll() => this.TextBox.SelectAll();

        public void ResetSelection() => this.TextBox.Select(0, 0);
    }
}
