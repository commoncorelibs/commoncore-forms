﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Windows.Forms;

namespace CommonCore.Forms
{
    public static class TextBoxExtensions
    {
        /// <summary>
        /// Appends a new line to the current text of a text box.
        /// <see cref="Environment.NewLine"/> is used to append the new line.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        public static void AppendLine(this TextBox self)
        {
            ThrowHelper.IfSelfNull(self);
            self.AppendText(Environment.NewLine);
        }

        /// <summary>
        /// Appends text followed by a new line to the current text of a text box.
        /// <see cref="Environment.NewLine"/> is used to append the new line.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="text">The text to append to the current contents of the text box.</param>
        public static void AppendLine(this TextBox self, string text)
        {
            ThrowHelper.IfSelfNull(self);
            self.AppendText(text);
            self.AppendText(Environment.NewLine);
        }
    }
}
